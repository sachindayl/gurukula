package com.example.quizapptesting.Services;

import com.example.quizapptesting.Model.EmotionAPI;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EmotionService {

    private final EmotionAPI emotionAPI;

    public EmotionService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://southeastasia.api.cognitive.microsoft.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        emotionAPI = retrofit.create(EmotionAPI.class);
    }

    public EmotionAPI getEmotionAPI() {
        return emotionAPI;
    }

}
