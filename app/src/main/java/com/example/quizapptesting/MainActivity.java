package com.example.quizapptesting;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.quizapptesting.Model.FaceDataModel;
import com.example.quizapptesting.Model.Question;
import com.example.quizapptesting.Services.EmotionService;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private static final int CAMERA_PERMISSION_CODE = 100;
    private static final int STORAGE_PERMISSION_CODE = 101;
    private static final String TAG = "Main Activity";
    Button b1, b2, b3, b4, takePhoto;
    TextView t1_question, timer;
    int total = 0;
    int correct = 0;
    DatabaseReference reference;
    private ImageCapture imageCapture = null;

    private ExecutorService cameraExecutor;
    private EmotionService emotionService;
    PreviewView imageView;

    int wrong = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = (PreviewView) findViewById(R.id.imageView);
        b1 = (Button) findViewById(R.id.Opt1);
        b2 = (Button) findViewById(R.id.Opt2);
        b3 = (Button) findViewById(R.id.Opt3);
        b4 = (Button) findViewById(R.id.Opt4);
        takePhoto = (Button) findViewById(R.id.takePhoto);
        emotionService = new EmotionService();

        t1_question = (TextView) findViewById(R.id.questionsTxt);
        timer = (TextView) findViewById(R.id.timer);
        checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);

        updateQuestion();
        reverseTimer(30, timer);
        takePhoto.setOnClickListener(v -> takePhoto());

    }

    public void checkPermission(String permission, int requestCode) {

        // Checking if permission is not granted
        if (ContextCompat.checkSelfPermission(
                MainActivity.this,
                permission)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat
                    .requestPermissions(
                            MainActivity.this,
                            new String[]{permission},
                            requestCode);
        } else {
            Toast
                    .makeText(MainActivity.this,
                            "Permission already granted",
                            Toast.LENGTH_SHORT)
                    .show();
            startCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super
                .onRequestPermissionsResult(requestCode,
                        permissions,
                        grantResults);

        if (requestCode == CAMERA_PERMISSION_CODE) {

            // Checking whether user granted the permission or not.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // Showing the toast message
                Toast.makeText(MainActivity.this,
                        "Camera Permission Granted",
                        Toast.LENGTH_SHORT)
                        .show();
                startCamera();
            } else {
                Toast.makeText(MainActivity.this,
                        "Camera Permission Denied",
                        Toast.LENGTH_SHORT)
                        .show();

                //TODO write code to block the user from using the quiz
            }
        } else if (requestCode == STORAGE_PERMISSION_CODE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(MainActivity.this,
                        "Storage Permission Granted",
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(MainActivity.this,
                        "Storage Permission Denied",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        }

    }

    private void startCamera() {
        ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);
        cameraProviderFuture.addListener((Runnable) () -> {
            ProcessCameraProvider cameraProvider = null;
            try {
                cameraProvider = cameraProviderFuture.get();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
            Preview preview = new Preview.Builder().build();
            preview.setSurfaceProvider(imageView.getSurfaceProvider());
            imageCapture = new ImageCapture.Builder().build();
            CameraSelector cameraSelector = CameraSelector.DEFAULT_FRONT_CAMERA;

            try {
                assert cameraProvider != null;
                cameraProvider.unbindAll();
                cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture);
            } catch (Exception e) {
                Log.e(TAG, "Use case binding failed", e);
            }

        }, ContextCompat.getMainExecutor(this));
    }

    private void takePhoto() {
        Log.v(TAG, "Clicked Take photo");
        File outputDirectory = MainActivity.this.getCacheDir();
        File photoFile = new File(outputDirectory, new SimpleDateFormat("ddMMyyyyHHmmss", Locale.UK).format(System.currentTimeMillis()) + ".jpg");
        ImageCapture.OutputFileOptions outputOptions = new ImageCapture.OutputFileOptions.Builder(photoFile).build();

        imageCapture.takePicture(outputOptions, ContextCompat.getMainExecutor(this), new ImageCapture.OnImageSavedCallback() {
            @Override
            public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                Uri savedUri = Uri.fromFile(photoFile);
                String msg = "Photo capture succeeded " + savedUri.toString();
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();

                try {
                    byte[] buffer = Files.readAllBytes(Paths.get(savedUri.getPath()));

                    RequestBody requestBody = RequestBody.create(MediaType.parse("application/octet-stream"), buffer);
                    Log.v(TAG, "pushing data");
                    Call<FaceDataModel[]> faceDataCall = emotionService.getEmotionAPI()
                            .getFaceData(
                                    requestBody,
                                    "detection_01",
                                    "recognition_04",
                                    new String[]{"emotion"},
                                    true,
                                    false
                            );

                    faceDataCall.enqueue(new Callback<FaceDataModel[]>() {
                        @Override
                        public void onResponse(Call<FaceDataModel[]> call, Response<FaceDataModel[]> response) {
                            FaceDataModel[] data = response.body();
                            // note to developer: you can hide visibility of the take photo button and put a timer to
                            //click it every few seconds.
                            // Note the response has like a 2 second delay from microsoft api
                            // get emotion data from here.
                            if (data != null) {
                                Toast
                                        .makeText(MainActivity.this,
                                                "Happiness: " + response.body()[0].getFaceAttributes().getEmotion().getHappiness(),
                                                Toast.LENGTH_SHORT)
                                        .show();
                                Log.v(TAG, "Emotion: " + response.body()[0].getFaceAttributes().getEmotion().getNeutral());
                            }
                        }

                        @Override
                        public void onFailure(Call<FaceDataModel[]> call, Throwable t) {
                            Log.e(TAG, "Network error" + t.getMessage(), t);
                            call.cancel();
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(@NonNull ImageCaptureException exception) {
                Log.e(TAG, "Photo capture failed: " + exception.getMessage(), exception);
            }
        });
    }

    private void updateQuestion() {
        total++;
        if (total > 4) {
            Intent i = new Intent(MainActivity.this, RActivity.class);
            i.putExtra("total", String.valueOf(total));
            i.putExtra("correct", String.valueOf(correct));
            i.putExtra("incorrect", String.valueOf(wrong));
            startActivity(i);

        } else {
            reference = FirebaseDatabase.getInstance().getReference().child("Questions").child(String.valueOf(total));
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    Question question = snapshot.getValue(Question.class);

                    t1_question.setText(question.getQuestion());
                    b1.setText(question.getOption1());
                    b2.setText(question.getOption2());
                    b3.setText(question.getOption3());
                    b4.setText(question.getOption4());

                    b1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (b1.getText().toString().equals(question.getAnswer())) {

                                b1.setBackgroundColor(Color.GREEN);

                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        correct++;
                                        b1.setBackgroundColor(Color.BLUE);

                                        updateQuestion();

                                    }
                                }, 10);
                            } else {
                                wrong = wrong + 1;
                                b1.setBackgroundColor(Color.RED);

                                if (b2.getText().toString().equals(question.getAnswer())) {
                                    b2.setBackgroundColor(Color.GREEN);
                                } else if (b3.getText().toString().equals(question.getAnswer())) {
                                    b3.setBackgroundColor(Color.GREEN);
                                } else if (b4.getText().toString().equals(question.getAnswer())) {
                                    b4.setBackgroundColor(Color.GREEN);

                                }
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        b1.setBackgroundColor(Color.BLUE);
                                        b2.setBackgroundColor(Color.BLUE);
                                        b3.setBackgroundColor(Color.BLUE);
                                        b4.setBackgroundColor(Color.BLUE);
                                        updateQuestion();
                                    }
                                }, 10);

                            }
                        }
                    });

                    b2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (b2.getText().toString().equals(question.getAnswer())) {
                                b2.setBackgroundColor(Color.GREEN);
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        correct++;
                                        b2.setBackgroundColor(Color.parseColor("#03A9F4"));
                                        updateQuestion();
                                    }
                                }, 10);
                            } else {
                                wrong = wrong + 1;
                                b2.setBackgroundColor(Color.RED);

                                if (b1.getText().toString().equals(question.getAnswer())) {
                                    b1.setBackgroundColor(Color.GREEN);
                                } else if (b3.getText().toString().equals(question.getAnswer())) {
                                    b3.setBackgroundColor(Color.GREEN);
                                } else if (b4.getText().toString().equals(question.getAnswer())) {
                                    b4.setBackgroundColor(Color.GREEN);

                                }
                                Handler handler = new Handler();
                                handler.postDelayed(() -> {
                                    b1.setBackgroundColor(Color.BLUE);
                                    b2.setBackgroundColor(Color.BLUE);
                                    b3.setBackgroundColor(Color.BLUE);
                                    b4.setBackgroundColor(Color.BLUE);
                                    updateQuestion();
                                }, 10);

                            }

                        }
                    });

                    b3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (b3.getText().toString().equals(question.getAnswer())) {
                                b3.setBackgroundColor(Color.GREEN);
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        correct++;
                                        b3.setBackgroundColor(Color.parseColor("#03A9F4"));
                                        updateQuestion();
                                    }
                                }, 10);
                            } else {
                                wrong = wrong + 1;
                                b3.setBackgroundColor(Color.RED);

                                if (b1.getText().toString().equals(question.getAnswer())) {
                                    b1.setBackgroundColor(Color.GREEN);
                                } else if (b2.getText().toString().equals(question.getAnswer())) {
                                    b2.setBackgroundColor(Color.GREEN);
                                } else if (b4.getText().toString().equals(question.getAnswer())) {
                                    b4.setBackgroundColor(Color.GREEN);

                                }
                                Handler handler = new Handler();
                                handler.postDelayed(() -> {
                                    b1.setBackgroundColor(Color.BLUE);
                                    b2.setBackgroundColor(Color.BLUE);
                                    b3.setBackgroundColor(Color.BLUE);
                                    b4.setBackgroundColor(Color.BLUE);
                                    updateQuestion();
                                }, 10);

                            }
                        }
                    });

                    b4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (b4.getText().toString().equals(question.getAnswer())) {
                                b4.setBackgroundColor(Color.GREEN);
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        correct++;
                                        b4.setBackgroundColor(Color.parseColor("#03A9F4"));
                                        updateQuestion();
                                    }
                                }, 10);
                            } else {
                                wrong = wrong + 1;
                                b4.setBackgroundColor(Color.RED);

                                if (b1.getText().toString().equals(question.getAnswer())) {
                                    b1.setBackgroundColor(Color.GREEN);
                                } else if (b2.getText().toString().equals(question.getAnswer())) {
                                    b2.setBackgroundColor(Color.GREEN);
                                } else if (b3.getText().toString().equals(question.getAnswer())) {
                                    b3.setBackgroundColor(Color.GREEN);

                                }
                                Handler handler = new Handler();
                                handler.postDelayed(() -> {
                                    b1.setBackgroundColor(Color.BLUE);
                                    b2.setBackgroundColor(Color.BLUE);
                                    b3.setBackgroundColor(Color.BLUE);
                                    b4.setBackgroundColor(Color.BLUE);
                                    updateQuestion();
                                }, 10);

                            }
                        }
                    });

                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
    }


    public void reverseTimer(int seconds, final TextView tv) {

        new CountDownTimer(seconds * 1000 + 1000, 1000) {


            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);
                int minites = seconds / 60;
                seconds = seconds % 60;
                tv.setText(String.format("%02d", minites) + ":" + String.format("%02d", seconds));
            }


            public void onFinish() {
                tv.setText("Complete");
                Intent myintent = new Intent(MainActivity.this, RActivity.class);
                myintent.putExtra("total", String.valueOf(total));
                myintent.putExtra("correct", String.valueOf(correct));
                myintent.putExtra("incorrect", String.valueOf(wrong));
                startActivity(myintent);

            }
        }.start();
    }
}