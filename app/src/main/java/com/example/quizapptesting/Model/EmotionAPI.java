package com.example.quizapptesting.Model;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface EmotionAPI {

    @Headers({
            "Content-Type: application/octet-stream",
            "Ocp-Apim-Subscription-Key:1fbcac82faa846029f99fc2ea49931bc"
    })
    @POST("/face/v1.0/detect")
    Call<FaceDataModel[]> getFaceData(
            @Body RequestBody bytes,
            @Query("detectionModel") String detection,
            @Query("recognitionModel") String recognition,
            @Query("returnFaceAttributes") String[] faceAttributes,
            @Query("returnFaceId") boolean faceId,
            @Query("returnFaceLandmarks") boolean faceLandmarks);
}
