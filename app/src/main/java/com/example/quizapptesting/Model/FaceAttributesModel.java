package com.example.quizapptesting.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FaceAttributesModel {
    @SerializedName("emotion")
    @Expose
    private EmotionModel emotion;

    public EmotionModel getEmotion() {
        return emotion;
    }

    public void setEmotion(EmotionModel emotion) {
        this.emotion = emotion;
    }
}
