package com.example.quizapptesting.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FaceDataModel {
    @SerializedName("faceId")
    @Expose
    private String faceId;
    @SerializedName("faceRectangle")
    @Expose
    private FaceRectangleModel faceRectangle;
    @SerializedName("faceAttributes")
    @Expose
    private FaceAttributesModel faceAttributes;

    public String getFaceId() {
        return faceId;
    }

    public void setFaceId(String faceId) {
        this.faceId = faceId;
    }

    public FaceRectangleModel getFaceRectangle() {
        return faceRectangle;
    }

    public void setFaceRectangle(FaceRectangleModel faceRectangle) {
        this.faceRectangle = faceRectangle;
    }

    public FaceAttributesModel getFaceAttributes() {
        return faceAttributes;
    }

    public void setFaceAttributes(FaceAttributesModel faceAttributes) {
        this.faceAttributes = faceAttributes;
    }

}
